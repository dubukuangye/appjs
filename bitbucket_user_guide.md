Bitbucket 使用指南
---

#### 搭建git环境

0. 安装[Git](http://msysgit.github.io/)
参考[如何在windows下安装GIT](http://jingyan.baidu.com/article/90895e0fb3495f64ed6b0b50.html)

1. 打开git bash
2. 切换到你本地存放代码的目录，比如C盘

```bash
cd c:
```

3. 克隆远程代码到本地文件夹，这里是C:/appjs

```bash    
git clone https://dubukuangye@bitbucket.org/dubukuangye/appjs.git appjs
```

4. 设置git用户名和邮箱。**下面黄色部分填自己的信息**

```bash
git config --global user.name "gqd"
git config --global user.email "12210240087@fudan.edu.cn"
```

#### 推送修改后的内容

* 以后有改动，放入C:/appjs/目录下。然后：
```bash
git pull
git add .
git commit -m "comment on this commit"
git push origin master
```
    第一步表示 "把远程代码拉到本地"
    第二步表示 “把修改推入暂存区”
    第三步表示 “本地提交这次修改” -m 后面可跟 **对这次提交的简短说明**
    第四步表示 “把本地代码推送到远程” 这里的远程指的就是 bitbucket 网站

#### 推荐资料

* 如果有兴趣，可以看看[廖雪峰的官方网站-Git教程](http://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000)

@(微信开发)